<?php

namespace Database\Seeders;

use App\Models\Account;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the current time
        $now = Carbon::now();

        $accounts=[
            [
                'title'=>'Individual',
                'created_at'=>$now,
                'updated_at'=>$now
            ],
            [
                'title'=>'Group',
                'created_at'=>$now,
                'updated_at'=>$now
            ],
            [
                'title'=>'Company',
                'created_at'=>$now,
                'updated_at'=>$now
            ],
            [
                'title'=>'Other',
                'created_at'=>$now,
                'updated_at'=>$now
            ],
        ];
        Account::insert($accounts);
    }
}
