<?php

namespace Database\Seeders;

use App\Models\AccountType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the current time
        $now = Carbon::now();

        $account_types=[
            [
            'title'=>'Ordinary Saving',
            'intrest'=>6.0,
            'created_at'=>$now,
            'updated_at'=>$now
            ],
            [
            'title'=>'Nari Bachat',
            'intrest'=>9.0,
            'created_at'=>$now,
            'updated_at'=>$now
            ],
            [
            'title'=>'Child Education Saving',
            'intrest'=>9.0,
            'created_at'=>$now,
            'updated_at'=>$now
            ],
            [
            'title'=>'Logic Special Saving',
            'intrest'=>9.0,
            'created_at'=>$now,
            'updated_at'=>$now
            ],
        ];
        AccountType::insert($account_types);
    }
}
