<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = ['name','email','phone','address','joined_date','salary','position','status'];

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
