<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'account_number', 'email', 'father_name', 'grandfather_name', 'address', 'dob', 'phone', 'gender', 'account_id', 'account_type_id', 'minor_id'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function account_type()
    {
        return $this->belongsTo(AccountType::class);
    }

    public function minor()
    {
        return $this->belongsTo(Minor::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function client_documents()
    {
        return $this->hasMany(ClientDocument::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function client_acount()
    {
        return $this->hasOne(ClientAccount::class);
    }
}
