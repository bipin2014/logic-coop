<?php

namespace Database\Seeders;

use App\Models\TransactionType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the current time
        $now = Carbon::now();

        $transaction_types=[
            [
            'name'=>'Deposit',
            'action'=>'INCREASE_BALANCE',
            'type'=>'CREDIT',
            'created_at'=>$now,
            'updated_at'=>$now
            ],
            [
            'name'=>'Withdraw',
            'action'=>'DECREASE_BALANCE',
            'type'=>'DEBIT',
            'created_at'=>$now,
            'updated_at'=>$now
            ] 
        ];
        TransactionType::insert($transaction_types);
    }
}
