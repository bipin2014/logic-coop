<?php

namespace App\Http\Controllers;

use App\Models\Minor;
use Illuminate\Http\Request;

class MinorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $minors = Minor::all();
        return response()->json([
            'data' => $minors,
            'status' => 'success',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'address' => 'required',
            'relation' => 'required'
        ]);

        $minor = Minor::create($request->all());

        return response()->json([
            'data' => $minor,
            'status' => 'success',
        ]);
    }
}
