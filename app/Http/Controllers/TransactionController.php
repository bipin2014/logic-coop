<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use App\Models\Client;
use App\Models\Transaction;
use App\Models\TransactionType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id)
    {
        $transactions = Transaction::where('client_id', $client_id)->get();
        return response()->json([
            'data' => $transactions,
            'status' => 'success',
        ]);
    }

    public function day_book()
    {
        // $transactions = Transaction::whereDate('transaction_date', date('Y-m-d'))->with('transaction_type')->get();
        $transactions = Transaction::select(DB::raw('sum(amount) as total_amount,transaction_type_id,employee_id'))->whereDate('transaction_date', date('Y-m-d'))->groupBy('transaction_type_id')->groupBy('employee_id')->with('transaction_type', 'employee')->get();
        return response()->json([
            'data' => $transactions,
            'status' => 'success',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request, $client_account_number)
    {
        $client = Client::where('account_number', $client_account_number)->first();
        $transaction = Transaction::create($request->validated() + ['client_id' => $client->id]);
        $this->handle_transaction_type_action($request->transaction_type_id, $client->id, $request->amount);
        return response()->json([
            'data' => $transaction,
            'status' => 'success',
        ]);
    }

    private function handle_transaction_type_action($transaction_type_id, $client_id, $amount)
    {
        $transaction_type = TransactionType::where('id', $transaction_type_id)->first();
        $client_account_controller = new ClientAccountController();
        if ($transaction_type->action == 'INCREASE_BALANCE') {
            $client_account_controller->increaseBalance($client_id, $amount);
        } else if ($transaction_type->action == 'DECREASE_BALANCE') {
            $client_account_controller->decreaseBalance($client_id, $amount);
        }
    }

    private function reverse_transaction_type_action($transaction_type_id, $client_id, $amount)
    {
        $transaction_type = TransactionType::where('id', $transaction_type_id)->first();

        $client_account_controller = new ClientAccountController();
        if ($transaction_type->action == 'INCREASE_BALANCE') {
            $client_account_controller->decreaseBalance($client_id, $amount);
        } else if ($transaction_type->action == 'DECREASE_BALANCE') {
            $client_account_controller->increaseBalance($client_id, $amount);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionRequest $request, $client_id, $id)
    {
        $transaction = Transaction::where('id', $id)->first();
        if ($transaction != null) {
            try {
                DB::beginTransaction();
                $this->reverse_transaction_type_action($transaction->transaction_type_id, $transaction->client_id, $transaction->amount);
                $this->handle_transaction_type_action($request->transaction_type_id, $transaction->client_id, $request->amount);
                $transaction->update($request->validated());
                DB::commit();
                return response()->json([
                    'message' => 'Transaction updated sucessfully',
                    'status' => 'success',
                ]);
            } catch (Exception $e) {
                DB::rollback();
                throw $e;
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($client_id, $id)
    {
        $transaction = Transaction::where('id', $id)->first();
        if ($transaction != null) {
            $transaction->delete();
            $this->reverse_transaction_type_action($transaction->transaction_type_id, $transaction->client_id, $transaction->amount);
            return response()->json([
                'message' => 'Transaction deleted sucessfully',
                'status' => 'success',
            ]);
        }
    }
}
