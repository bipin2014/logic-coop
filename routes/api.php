<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransactionTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get_user', [ApiController::class, 'get_user']);

    Route::apiResource('clients', ClientController::class);
    Route::apiResource('accounts', AccountController::class);
    Route::apiResource('accounttypes', AccountTypeController::class);
    Route::apiResource('transactiontypes', TransactionTypeController::class);
    Route::apiResource('employees', EmployeeController::class);

    Route::get('clients/{client_id}/transactions', [TransactionController::class,"index"]);
    Route::get('transactions/daybook', [TransactionController::class, "day_book"]);
    Route::post('clients/{client_id}/transactions', [TransactionController::class,"store"]);
    Route::put('clients/{client_id}/transactions/{id}', [TransactionController::class,"update"]);
    Route::delete('clients/{client_id}/transactions/{id}', [TransactionController::class,"destroy"]);
});
