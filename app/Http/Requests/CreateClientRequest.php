<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'account_number'=>'required',
            'account_id'=>'required',
            'account_type_id'=>'required',
            'employee_id'=>'required',
            'email' => 'email:rfc,dns',
            'gender' => 'sometimes',
            'phone' => 'sometimes',
            'dob' => 'date|before:'.date('m/d/Y'),
            'father_name'=> 'sometimes',
            'grandfather_name'=> 'sometimes',
            'address'=> 'sometimes'
        ];
    }
}
