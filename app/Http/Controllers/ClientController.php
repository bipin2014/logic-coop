<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClientRequest;
use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Models\ClientAccount;
use App\Models\Minor;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ClientResource::collection(Client::with('account','account_type', 'client_acount')->get());
    }

    
    public function store(CreateClientRequest $request)
    {
        $minor_id=null;
        if ($request->minor) {
            // $minor=Minor::create($request->minor->only('name','address','relation'));
            $minor=Minor::create([
                'name'=>$request->minor["name"],
                'address'=>$request->minor["address"],
                'relation'=>$request->minor["relation"],
            ]);
            $minor_id=$minor->id;
        }
       $client=Client::create($request->validated()+['minor_id' => $minor_id]);
       ClientAccount::create([
            'client_id'=>$client->id,
       ]);
       return new ClientResource($client);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
